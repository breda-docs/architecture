.. Breda documentation master file, created by
   sphinx-quickstart on Mon Jan 25 15:35:06 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Breda's documentation!
=================================

Breda is a platform for distributed ledger solutions underpinned by a modular architecture delivering
high degrees of confidentiality, resiliency, flexibility, and scalability.

It is designed to support pluggable implementations of different components and accommodate the complexity and
intricacies that exist across the economic ecosystem.



.. toctree::
   :caption: Architecture
   :maxdepth: 2

   pages/blockchain
   pages/architecture/system
   pages/bootstrap
   pages/bpu

.. toctree::
   :caption: Services
   :maxdepth: 2

   pages/architecture/services/ca
   pages/architecture/services/discovery
   pages/architecture/services/bastions
   pages/architecture/services/oracle
   pages/architecture/services/vault
   pages/architecture/services/vault_sync

.. toctree::
   :caption: Protocols
   :maxdepth: 2

   pages/architecture/protocols/gossip
   pages/architecture/protocols/kad





Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

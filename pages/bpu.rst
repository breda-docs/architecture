BPU
===

The BPU is a graphical representation of business processes. It is mainly inspired by BPMN in its version 2.

The graphic representation of a BPU process corresponds to a series of smart contracts that will be executed
from the Breda network.

A representation of a BPU business process is mainly composed of:

- **Events**
    an event is a representation of something that occurs within the process.
    There are three types of events:

    - **Start event**
    as the word says these events collect the information of the beginning of a process

    - **Intermediate Event**
    This is what happens between the start event and the end event

    - **End Event**
    as the word says, represents the end of a process

- **Activity**
    The activity represents the type of action to be carried out within the process. In BPUs we currently have only one type of activity:

    - **Task**
    it is a unit of work and cannot be divided into sub-task.

    - **Gateway**
    Gateways determine the forking or merge of tasks within a BPU process. In Breda we have:

    - **Exclusive**
    Used to create a unique alternative in the process flow. An example could be an IF / Else

    - **Event Based**
    It is activated only if there is a specific event that occurs within the flow.

    - **Parallel**
    It splits the flow into two or more parallel processes and there is no conditional evaluation.
    (The gateway is still in study phase due to the difficult integration with the principles of blockchain).

A representation of a process could be this:

.. uml::

    (*) -right->[Receive and Event from CRM] "Send Notification Email"
    -right->[Check Inventory] "Get the Part ID"
    if "The part Exist" then
        -->[true] "Start Repairing Process"
        --> "Send Notification to Warehouse"
        -right-> (*)
    else
        ->[false] "Log the Fail"
        -->[Notify the CRM about the Fail] "Notify the CRM"
        --> (*)
    endif


This is obviously a fairly simple representation of a repair process, but ipothetically we imagine that:

- An event arrives from the company's CRM that informs that a part with a specific ID must be repaired.
- The system automatically checks the database if the ID is in the parts inventory.
- If the part exists in the inventory, a notification is sent to the repair center that there is something to repair
- If the part does not exist, a log must be stored with the Timestamp and the operation that has occurred and then
    the CRM must be informed again that the part ID does not exist.
- The process ends

BPU Process Creation
--------------------

the presentation of a business process in BPU is always through xml code, below we will describe the file format,
but it is possible to extract the content from a graphical representation.

.. image:: /imgs/bpmn-js.gif

This is the main reason why a solution such as the BPU has been studied to create what are the Smart Contracts of Breda.
With a graphic representation, users who do not have technical preparation and who know well the flow of a process,
will have easy the creation of a Smart Contract

Before going into detail in the description of the BPU file format, it is necessary to briefly explain what a
Smart Contract is.

Smart Contract
^^^^^^^^^^^^^^
A smart contract is a computer program or a transaction protocol which is intended to automatically execute,
control or document legally relevant events and actions according to the terms of a contract or an agreement.
The objectives of smart contracts are the reduction of need in trusted intermediators, arbitrations and enforcement
costs, fraud losses, as well as the reduction of malicious and accidental exceptions.

This is the most generic definition but, for better detail, we can say that a Smart Contract is a series of instructions
(code) that the Breda network launches (executes) when events occur.

These events will be connected to a Smart Contract (it is possible to connect one or more events to a single Smart
Contract) and each time something internal or external to Breda launches one of these events, Breda will execute
the corresponding code.

As we have explained before, the BPU flow is composed mainly of *events* and *tasks*, since in Breda, Smart Contracts
are the Tasks of a process

XML Format
^^^^^^^^^^

The XML version of this business process unit BPU have this representation

.. code-block:: javascript

    <definitions id="definitions">

        <process id="financialReport" name="Monthly financial report reminder process">

            <startEvent id="theStart" />

            <sequenceFlow id="flow1" sourceRef="theStart" targetRef="writeReportTask" />

            <userTask id="writeReportTask" name="Write monthly financial report" >
                <documentation>
                  Write monthly financial report for publication to shareholders.
                </documentation>
                <dueDate>1234455676777</dueDate>
                <potentialOwner>
                  <resourceAssignmentExpression>
                    <formalExpression name="user">456789000</formalExpression>
                    <formalExpression name="group">12345</formalExpression>
                  </resourceAssignmentExpression>
                </potentialOwner>
                <humanPerformer>
                  <resourceAssignmentExpression>
                    <formalExpression>1234567</formalExpression>
                  </resourceAssignmentExpression>
                </humanPerformer>
            </userTask>

            <sequenceFlow id="flow2" sourceRef="writeReportTask" targetRef="verifyReportTask" />

            <serviceTask id="verifyReportTask" package="org.services.custom" class="report" name="Verify monthly financial report" />

            <sequenceFlow id="flow3" sourceRef="verifyReportTask" targetRef="sendMail" />

            <serviceTask id="sendMail" package="org.services.email" method="send">

            <sequenceFlow id="flow4" sourceRef="sendMail" targetRef="theEnd" />

            <endEvent id="theEnd" />

        </process>
    </definitions>

The xml document contains all the commands, the connections of the whole process.

The document begins with the root tag *<definition>*.
All BPU documents must start with this root.

Then you have to define the process, with the *<process>* tag, there we define the name of the process and
a brief description.

Inside the process tag we are going to introduce the process flow.

The sequenceFlow tag determines in which direction the process has to go.
the sourceRef and targetRef tags indicate:

1. *sourceRef* is the reference to the previous event / task
2. *targetRef* is the reference to the next event / task


Tasks
-----

User Task
^^^^^^^^^

**Description**

A 'user task' is used to model work that needs to be done by a human.
Need the verification of the Owner and the User that execute the task

**XML representation**

A user task is defined in XML as follows. The id attribute is required, the name attribute is optional.

.. code-block:: javascript

    <userTask id="theTask" name="Important task" />


A user task can also have a description
A description is defined by adding the documentation element.

.. code-block:: javascript

    <userTask id="theTask" name="Schedule meeting" >
    <documentation>
        Schedule an engineering meeting for next week with the new hire.
    </documentation>

as well each task has a field indicating the due date of that task.

**User assignment**

A user task can be directly assigned to a user. This is done by defining a humanPerformer sub element.
Such a humanPerformer definition needs a resourceAssignmentExpression that actually defines the user hash.
Currently, only formalExpressions are supported.

.. code-block:: javascript

    <process >

    ...

        <userTask id='theTask' name='important task' >
            <humanPerformer>
              <resourceAssignmentExpression>
                <formalExpression>kermit</formalExpression>
              </resourceAssignmentExpression>
            </humanPerformer>
        </userTask>

is possible to add more than one <humanPerformer> tag

another asignement is the owner of that task,

.. code-block:: javascript

    <potentialOwner>
      <resourceAssignmentExpression>
        <formalExpression name="user">456789000</formalExpression>
        <formalExpression name="group">12345</formalExpression>
      </resourceAssignmentExpression>
    </potentialOwner>

when the task is completed the owner must be informed and give the result of the task

Service Task
^^^^^^^^^^^^

**Description**
A service task is used to invoke an external Breda Python class.

**XML representation**

.. code-block:: javascript

    <serviceTask id="verifyReportTask" package="org.services.custom" class="report" name="Verify monthly financial report" />


to invoke the service is necesary to configure

1. the package attribute, that define the internal package

and one of the following options:

1. class attribute, call the Service Class correspondant

2. method attribute, call a specific python function


Events
------

Events are used to model something that happens during the lifetime of a process.

Signal Event Definitions
^^^^^^^^^^^^^^^^^^^^^^^^

Signal events are events that reference a named signal. A signal is an event of global scope (broadcast semantics)
and is delivered to all active handlers (waiting process instances/catching signal events).

A signal event definition is declared using the signalEventDefinition element.

The attribute signalRef references a signal element declared as a child element of the definitions root element.

The following is an excerpt of a process where a signal event is thrown and caught by intermediate events.

.. code-block:: javascript

    <definitions... >
        <!-- declaration of the signal -->
        <signal id="alertSignal" name="alert" />

        <process id="catchSignal">
            <intermediateThrowEvent id="throwSignalEvent" name="Alert">
                <!-- signal event definition -->
                <signalEventDefinition signalRef="alertSignal" />
            </intermediateThrowEvent>
            ...
            <intermediateCatchEvent id="catchSignalEvent" name="On Alert">
                <!-- signal event definition -->
                <signalEventDefinition signalRef="alertSignal" />
            </intermediateCatchEvent>
            ...
        </process>
    </definitions>

Message Event Definitions
^^^^^^^^^^^^^^^^^^^^^^^^^

Message events are events that reference a named message. A message has a name and a payload. Unlike a signal,
a message event is always directed at a single receiver.

A message event definition is declared using the messageEventDefinition element.

The attribute messageRef references a message element declared as a child element of the definitions root element.

The following is an excerpt of a process where two message events is declared and referenced by a start event and an
intermediate catching message event.

.. code-block:: javascript

    <definitions id="definitions">

        <message id="newInvoice" name="newInvoiceMessage" />
        <message id="payment" name="paymentMessage" />

      <process id="invoiceProcess">

        <startEvent id="messageStart" >
            <messageEventDefinition messageRef="newInvoice" />
        </startEvent>
        ...
        <intermediateCatchEvent id="paymentEvt" >
            <messageEventDefinition messageRef="payment" />
        </intermediateCatchEvent>
        ...
      </process>

    </definitions>

Start Events
^^^^^^^^^^^^

A start event indicates where a process starts.

The type of start event (process starts on arrival of message, on specific time intervals, and so on),
defining how the process is started, is shown as a small icon in the visual representation of the event.

In the XML representation, the type is given by the declaration of a sub-element.

Start events are always catching: conceptually the event is (at any time) waiting until a certain trigger happens.

.. code-block:: javascript

    <startEvent id="theStart" />

The Start Event have an optional attribute called *initiator* that is a reference of a Python Method

End Events
^^^^^^^^^^

An end event signifies the end of a path in a process or sub-process. An end event is always throwing.

This means that when process execution arrives at an end event, a result is thrown.

The type of result is depicted by the inner black icon of the event.

In the XML representation, the type is given by the declaration of a sub-element.
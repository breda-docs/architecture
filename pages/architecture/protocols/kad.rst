Kademlia Protocol
=================

Kademlia is a peer-to-peer distributed hash table (DHT), the distributed nature of Kademlia means that there is
no absolute truth where NodeIDs are mapped to their address (i.e. — the routing table is distributed), so each node
must keep this mapping for a subset of the nodes on the network in its own routing table. Kademlia-based networks are
highly resistant to denial of service attacks and the loss of a group of nodes as the protocol simply routes around the
unavailable nodes.Kademlia’s big breakthrough was to minimize internode messaging through its use of XOR metric as a
means to define distance between points in the key space. Thus, if the distance is expressed as log2(n) nodes, this
means that for a network with 10,000,000 Breda nodes, only about 20 hops would be necessary at most for communication
with any subset of nodes.

.. image:: /imgs/kademlia_net.png
   :align: center

the identification of a node within the Kademlia network of Breda takes place by the NodeID. this value should not
be confused with the node ID within the blockchain, which derives directly from the public key.
the NodeID in kademlia is a 160-bit string (SHA-1) obtained from the public key, and its position is determined by
the shortest unique prefix of its ID. To assign key-value pairs to particular nodes, Breda relies on a notion of
distance between two identifiers. Given two 160-bit identifiers, x and y, Kademlia in Breda defines the distance
between them as the XOR. From a node point of view, the tree is divided into series of successive sub-trees where the
160th subtree contains the individual node. The Kademlia protocol ensures that each node knows of at least one node on
each of its sub-trees. With this guarantee, a node can locate any other node by its ID.

Gossip Protocol
===============

A gossip protocol is a procedure or process of computer peer-to-peer communication that is based on the way
epidemics spread.

Rumours in society travel at a great speed and reach almost every member of the community, and which is even
better without needing a central coordinator.

It is a basic result of epidemic theory that simple epidemics eventually infect the entire population.
The theory also shows that starting with a single infected site this is achieved in expected time proportional to
the log of the population size. Gossip protocols try to solve the problem of Multicast, that is, we want to communicate
a message to all the nodes in the network. Although as we’ll see, each node doesn’t send the message to all nodes, each
node sends the message to only a few of the nodes.

In Breda we are using Gossip protocol for different reasons:

- Scalable

  They are scalable because in general, it takes O(logN) rounds to reach all nodes, where N is number of nodes.
  Also each node sends only a fixed number of messages independent of the number of nodes in the network.
  A node does not wait for acknowledgments, and it doesn’t take any recovery action if an acknowledgment does not
  arrive. A system can easily scale to millions of processes.

- Fault-tolerance

  They have the ability to operate in networks with irregular and unknown connectivity. They work really well in
  these situations because as we’re going to se a node shares the same information several times to different nodes,
  so if a node is not accesible the information is shared anyways through a different node. In other words there are
  many routes by which information can flow from its source to its destinations.

- Robust

  No node plays a specific role in the network, so a failed node will not prevent other nodes from continuing
  sending messages.

  Each node can join or leave whenever it pleases without seriously disrupting the system’s overall quality of service.

- Convergent consistency

  Gossip protocols achieve exponentially rapid spread of information and, therefore, converge exponentially
  quickly to a globally consistent state after a new event occurs, in the absence of additional events. Propagate any
  new information to all nodes that will be affected by the information within time logarithmic in the size of the
  system.

- Extremely decentralized

  Gossip offers an extremely decentralized form of information discovery, and its latencies are often acceptable
  if the information won’t actually be used immediately.

Implementation
--------------

Gossip protocols are very simple conceptually and very simple to code.  The basic idea behind them is this: A node
wants to share some information to the other nodes in the network. Then periodically it selects randomly a node from
the set of nodes and exchanges the information. The node that receives the information does exactly the same thing.
The information is periodically send to N targets, N is called fanout.

.. image:: /imgs/work1080.gif
   :align: center

in the Breda Gossip protocol there are two fundamental concepts:

- Cycle
  Number of rounds to spread a rumour

- Fanout
  Number of nodes a node gossips with in each cycle. When a node wants to broadcast a message,
  it selects t nodes from the system at random and sends the message to them.

With fanout = 1, then O(Log N) cycles are necessary for the update to reach all the nodes.

In most implementations a node has a partial view of the nodes, in order to keep a completed view of the nodes,
each node should have stored the whole list and it should be able to update the list constantly, for example if a
node is inaccessible it should be removed from the set in each other node, which is a very complex task.

Node States
-----------

There are three states during the life cycle of a node:

- Infective
  A node with an update it is willing to share

- Susceptible
  A node that has not received the update yet (It is not infected).

- Removed
  A node that has already received the update but it is not willing to share it.

Removed is trickier than rest. It’s not easy to determine when a node should stop sharing the info/update. Ideally a
node should stop sharing the update when all the nodes is linked with have the update. But that would mean that node
would have to have knowledge of the status of the other nodes.

There are various ways of distributing information in an epidemic manner and each of these has a different
method of deciding when a node is in the Removed state or not:

- Direct mail
  The one they had in place initially, each new update is immediately emailed from its entry site to all
  other sites but it presented several problems:
  - The sending node was a bottleneck O(n).

  - Each update was propagated to all the nodes, so each node had to know all the nodes in the system.

  - Messages could be discarded when a node was unresponsive for a long time or due to queue overflow.

- Anti-entropy
  Every site regularly chooses another site at random and by exchanging database contents with it resolves any
  differences between the two. It was used together with direct mail or Rumour Mongering and run to correct
  undelivered updates, kind of a backup system.

  Image that using Direct Mail an updated did not arrive to a node, Anti-entropy was supposed to fix that.

- Rumor mongering
  Sites are initially ignorant; when a site receives a new update it becomes a “hot rumor”; while a site holds
  a hot rumor. It periodically chooses another site at random and ensures that the other site has seen the update.
  when a site has tried to share a hot rumor with too many  sites that have already seen it, the site stops treating
  the rumor as hot and retains the update without propagating.

it is clear that the Rumor Mongering method seems to be the one that best suits the Breda network.

To define the status of Removed the following algorithm must be followed:

.. code-block:: console

    log(n)(base f) = c

the variables are:

- f = fannout

- n = total nodes in the network

- c = number of cycles

this formula returns the number of cycles that each node must perform to position itself in the Removed state

for example if in our network we have:

- 40 nodes

- Each node has a fanout of 4

- Each node must do 2.66 cycles, which rounded up to the next number are 3

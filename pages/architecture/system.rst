System Architecture
===================

Summary
-------

The Breda network is a group of organizations, connected to each other through gateways called Bastions.
A network is made by one or more Organizations.

Network Structure
-----------------

A Breda network is essentially made up of Organizations and they are all connected to each other.
The division into Organizations as physical components of the network allows to create a real segregation
of the data, thus allowing each organization to own the data it generates.

A complete Breda Network, will contain:

1. One or more organizations

.. image:: /imgs/small_organization.png

2. One :doc:`Discovery Service </pages/architecture/services/discovery>`

.. image:: /imgs/small_discovery.png

3. One :doc:`CA Service </pages/architecture/services/ca>`

.. image:: /imgs/small_ca.png


the CA and the Discovery services,  have the task of identifying and directing the components that will become
part of an organization.

Every new member of an organization, will first have to request access to the CA and subsequently will have to
request the main information of the organization from Discovery.

.. note:: Later we will see the Breda Authentication and Authorization processes


Organization Structure
----------------------

Organization is the essential component of the Breda network.

An Organization can represent, in reality, a nation, a company or even a department of a company.

The organization is a physical structure, which contains a complete blockchain network nodes and a series of
additional services.

.. image:: /imgs/organization_services.png

A full Organization contain:

1. One or more :doc:`Applications </pages/architecture/application>`
2. One or more :doc:`Light Nodes </pages/architecture/node>`
3. One or more :doc:`Full Nodes </pages/architecture/node>`
4. Minimum 4 :doc:`Validators </pages/architecture/node>`
5. One or More :doc:`Bastions Services </pages/architecture/services/bastions>`
6. One :doc:`Vault Database </pages/architecture/services/vault>`
7. One or more :doc:`Oracle Services </pages/architecture/services/oracle>`

Network Authorization
---------------------

As previously mentioned, the Breda network is authorized and private, therefore any component must be
cryptographically recognized.

The authorization process follows the auth_certificate_ standard and the x509_ standard.
The network service responsible for authorizing the components is the :doc:`CA Service </pages/architecture/services/ca>`.
The :doc:`CA Service </pages/architecture/services/ca>` must be previously aware of a series of information before
being able to enter service.

The configuration parameters are:

1. List of public keys corresponding to the components that will be authorized. This configuration parameter can also be modified on hot.

2. List of organizations identifiers. Normally this parameter corresponds to the name you want to give to an organization.

3. Certificate creation parameter with expiration period. This parameter is a bulean, if it is True, the :doc:`CA Service </pages/architecture/services/ca>` will create a certificate for each component with the expiration time, it will therefore be necessary to configure the validity time parameter of the certificate.

Every Component must ask for Authorization and must follow the same rules.

Authorization Flow
^^^^^^^^^^^^^^^^^^

Once the CSR has been created, the component must send it to the :doc:`CA Service </pages/architecture/services/ca>`.
The Component must know some information of the :doc:`CA Service </pages/architecture/services/ca>` in advance:

1. The public ip

2. The public key

The :doc:`CA Service </pages/architecture/services/ca>`, once the CSR has been received, will begin the Authorization flow.

.. uml::

    participant "Node" as node
    participant "CA Service" as ca

    node -> node : generate CSR
    node -> ca : CSR cert
    ca -> ca: generate and sign the CRT
    ca --> node: CRT

The process is quite simple:

1. The node generate a CSR certificate with all the informations needed
2. The node send the CSR to the :doc:`CA Service </pages/architecture/services/ca>`
3. The :doc:`CA Service </pages/architecture/services/ca>` carries out the necessary checks
4. The :doc:`CA Service </pages/architecture/services/ca>` send back the CRT signed

Routing Flow
^^^^^^^^^^^^

Once the authorization process is completed, the component must be enrouted.

The component will have to call a service external to the organizations called :doc:`Discovery Service </pages/architecture/services/discovery>`
which as DNS will return a series of information concerning the organization of the component and the list of peers.

Discovery receives the certificate signed by the CA from the component.
the certificate contains the information that Discovery uses to obtain the information to be returned.
The Organization field of the certificate informs about the membership organization while the Organization Unit informs about the type of component.
With the return data the component will finally have the peers list of his own organization.

.. uml::

    component "Node" as nod
    component "Discovery Service" as disc
    node "Organization" as org

    nod -> disc

.. uml::

    component "Discovery Service" as disc
    node "Organization" as org {
        component "Node" as node
    }


.. _auth_certificate: https://en.wikipedia.org/wiki/Authorization_certificate
.. _x509: https://en.wikipedia.org/wiki/X.509
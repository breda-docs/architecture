Vault Syncronization
====================

Before identifying the synchronization method of the Vault, it is necessary to delve into the type of data deals with.

There are mainly two data formats:

1. The data coming from the blockchain chain

2. Data external to the blockchain chain

Data from Blockchain
--------------------

the blockchain uses transactions exclusively, each transaction is an envelope that contains the data, in binary format.

the blockchain uses transactions exclusively, each transaction is an envelope that contains the data, in binary format.
As it is easy to guess, this type of information formatting is not the best when it is necessary to perform queries to
obtain information. The Vault, therefore, must be able to:

1. receive the transactions that take place inside the blockchain

2. extract the data inside the transaction, validate it with the data model managed by the Vault and introduce it into
the database (relational or non-relational), to facilitate queries.


The process is very similar to the design of a CQS (Command Query Service) pattern.

the blockchain, by its nature, is actualized with events (transactions), so the general principle is that every time a
transaction is moved from the pending pool to the transacion pool, an event is sent.

For the occasion, an external "message broker" system will be used.

This Broker have to manage these events launched by the blockchain management service.

.. uml::

    box "Blockchain" #LightBlue
    participant "Consensus" as consensus
    participant "Pending Pool" as pending
    participant "Pool" as pool
    participant "Service" as service
    end box
    participant "Broker" as broker
    box "Vault" #yellow
    participant "Service" as vault_service
    participant "Database" as vault_db
    end box
    consensus -> pending: Get Pending transactions
    consensus -> consensus: Gen a new block
    consensus -> pool: move Trans. to the pool
    service -> broker: send Trans. as event
    broker -> vault_service: get Event
    vault_service -> vault_service: trait the event
    vault_service -> vault_db: update the database

The updating process follow the rules of CQS Pattern

CQS Pattern
^^^^^^^^^^^

The CQS (pattern says that object’s methods should group into:

1. Commands: can change state of the system and does not return any value.
2. Queries: cannot change state of the system (are free of side effects) and return a value.

This clear separation is useful for developers.

Here’s why: queries and commands are fundamentally different operations. They have different scalability
characteristics, so it’s useful to decouple them. Queries can be repeated; they are safe and idempotent.
(That is to say asking a query a second time doesn’t change the answer.) You can process different orders of
queries without worrying about consistency. By contrast, commands are not safe to repeat.

The order of commands matters.

.. image:: /imgs/cqrs.png




Discovery Services
==================

discovery is a DNS service within the Breda network. The information interchange protocol is custom and allows
to en route the components of the Breda network within the different organizations.

Interfaces
----------

The main communication interface is REST with TLS

the main principal for a Discovery Service is:

1. register a new node into an internal database
2. enroute the new node into a specific Organization

Endpoints
^^^^^^^^^

There are two access points to the service

1. Subscribe: https://discovery-domain/subscribe

2. Update: https://discovery-domain/update

Subscribe
^^^^^^^^^

.. csv-table::
   :header: "Method", "Parameters", "Response"
   :widths: 5, 10, 30

    "POST", "cert: base 64 string", "200:JSON: [{ip:127.0.0.1, pk:base64 string"

the subscribe method allows you to subscribe the component within the Discovery, once you have obtained the certificate
it will return a list in JSON format with the peers of the organization related to the certificate,
the maximum number of peers to return is preconfigured by the system administrator.
this is the flow:


.. uml::

    participant "Application" as app
    participant "REST" as rest
    box "Diswcovery Service" #LightBlue
    participant "REST" as ds_rest
    participant "Service" as ds_service
    participant "validator" as ds_val
    participant "configuration" as ds_conf
    database "Table Nodes" as ds_table
    end box

    app -> rest : POST
    rest -> ds_rest: subscribe POST <<cert>>
    ds_rest -> ds_val: validate cert
    ds_val -> ds_conf: checkPK
    ds_conf --> ds_val: true/false
    ds_val -> ds_conf: checkOrg
    ds_conf --> ds_val: true/false
    ds_val --> ds_rest
    ds_rest -> ds_table: update
    ds_rest -> ds_service: get list neigh.
    ds_service -> ds_table: select query
    ds_service -> ds_service: prepare list
    ds_service --> ds_rest
    ds_rest --> rest: list neigh.
    rest --> app

The subscription process is quite clear from the graph.
The customer sends a POST with the certificate just obtained from the CA Service, the Discovery Service first checks
if the certificate is valid by validating the public key of the Issuer with the value preconfigured in the
configuration, then validating the signature to check that the contents of the certificate is valid.

If the validation process is correct, we proceed to extract the information from the Subject field of the certificate.
You only need to check the Organization and the Organization Unit fields. At this point, a query must be made to the
database to obtain a list (the number of peers has been previously introduced in the service configuration) of the
peers that belong to the Organization and that are of the same Organization Unit.

The final Json string will have more information:

- If present the PK and the ip of the validators

- If present the PK and the IP of the Oracle nodes

Update
^^^^^^

.. csv-table::
   :header: "Method", "Parameters", "Response"
   :widths: 5, 10, 30

    "POST", "ip: string / port: int / cert: string", "200"
    "GET", "version: int/optional / cert: string", "200 OK {list of nodes}"


The update process keeps the Discovery Service database up-to-date at all times.

In case a service/node modifies its network information, the discovery will receive an update.

The update, also with the HTTP GET command to return the list of the last services / nodes that have modified
their information. This endpoint is called regularly by the rest of the services / nodes to check if
their database is updated..

the flow is:

.. uml::

    participant "Application" as app
    participant "REST" as rest
    box "Diswcovery Service" #LightBlue
    participant "REST" as ds_rest
    participant "validator" as ds_val
    participant "configuration" as ds_conf
    database "Table Nodes" as ds_table
    end box

    app -> rest : POST
    rest -> ds_rest: update POST <<cert/ip/port>>
    ds_rest -> ds_val: validate cert
    ds_val -> ds_conf: checkPK
    ds_conf --> ds_val: true/false
    ds_val -> ds_conf: checkOrg
    ds_conf --> ds_val: true/false
    ds_val --> ds_rest
    ds_rest -> ds_table: update
    ds_rest --> rest: 200 Ok
    rest --> app


for the GET process the flow is:

.. uml::

    participant "Application" as app
    participant "REST" as rest
    box "Diswcovery Service" #LightBlue
    participant "REST" as ds_rest
    participant "Service" as ds_service
    participant "validator" as ds_val
    participant "configuration" as ds_conf
    database "Table Nodes" as ds_table
    end box

    app -> rest : GET
    rest -> ds_rest: get <<version/cert>>
    ds_rest -> ds_val: validate cert
    ds_val -> ds_conf: checkPK
    ds_conf --> ds_val: true/false
    ds_val -> ds_conf: checkOrg
    ds_conf --> ds_val: true/false
    ds_val --> ds_rest
    ds_rest -> ds_service: check the list is updated
    ds_service -> ds_table
    ds_service --> ds_rest: eventualy return a list of nodes
    ds_rest --> rest: 200 Ok / list
    rest --> app
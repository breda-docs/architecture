Vault Database
==============

A Vault is a database with two main interfaces

1. REST

2. Binary ( drivers )

In general there are no specifications for the type of database, but as a default solution the use of PostgreSQL
is foreseen.
the database vault allows you to create serverless applications because it offers a data model interface and all
CRUD commands.

REST
----

the REST interface allows you to create a real Object Storage. Calls to the interface must be authenticated with
JWT tokens.

Auth
^^^^

The authorization and authentication process takes place through a JWT token.
the token consists of:

1. payload: node/user public key

2. secret: present in the node and database vault configuration

3. algorithm: HS256

the JWT token must be sent as the HTTP request header with the following format:

.. code-block:: console
    Authorization: Bearer <token>

.. uml::

    participant "Application" as app

    box "Vault Service" #LightBlue
    participant "REST" as vl_rest
    participant "Auth" as vl_auth
    database "Database" as vl_db
    end box

    app -> app : generate JWT
    app -> vl_rest: CRUD call
    vl_rest -> vl_auth: verify JWT
    vl_auth --> vl_rest
    vl_rest -> vl_db: Query DB
    vl_rest --> app: return Data


Rest Methods
^^^^^^^^^^^

the interface rest uses the classic HTTP commands to perform the CRUD operations on the database.

.. csv-table::
   :header: "Method", "CRUD Command", "Description"
   :widths: 5, 10, 30

    "POST", "ADD", "this command add an object to the database"
    "GET", "SELECT", "this command return the information related with the object"
    "PUT", "UPDATE", "this command update the object"
    "DELETE", "DELETE", "this command delete the object"


Rest Format
^^^^^^^^^^^

the data is represented directly by the url of the Vault database

.. code-block:: console

    http://vaultdatabase/datavalue/parameters

as we said before, it is not necessary to indicate the command to be executed because the HTTP method is the one that identifies the CRUD
For example, if we wanted to get information about an aircraft component, for example an engine.

.. code-block:: console

    GET - http://vaultdatabase/engine/1234

in the url we see identified the data model we want to know "engine" and then we pass the information of the corresponding ID to the engine.

It is also possible to add parameters to improve the quality or return information of the query.

.. csv-table::
   :header: "Parameter", "Description"
   :widths: 5, 10

    "foreign", "this command instructs the query whether to perform a search for the keys connected to the return data, such as foreign keys, for example"

for example:

.. code-block:: console
    GET - http://vaultdatabase/engine/1234?foreign=true

REST Data Return
^^^^^^^^^^^^^^^^

the Vault database always returns a JSON data format.

.. code-block:: javascript

    {
        engine: "Rolls Royce"
        id: 2345
        serial_number: "AD-123456"
    }

if the foreign parameter is present, the return JSON value would be composed of an array.

.. code-block:: javascript

    {
        engine: "Rolls Royce"
        id: 2345
        serial_number: "AD-123456"
        parts: [
            {
               type: "fun",
               serial_number: "AC-2345"
            }
            ...
        ]
    }
Certificate Autority Service
============================

In BREDA the CA (Certificate Authority) service complies with the X509 standards.

The CA service is an entity that issue digital certificates for the components of Organizations.

A digital certificate certifies the ownership of a public key by the named subject of the certificate.
This allows others (relying parties) to rely upon signatures or on assertions made about the private key that
corresponds to the certified public key. A CA acts as a trusted third party—trusted both by the subject (owner) of the
certificate and by the party relying upon the certificate. The format of these certificates is specified by the X.509.

External Interfaces
-------------------

The CA Service uses a REST-like interface.
communication takes place via HTTPS (TLS) with a certificate previously provided by the system administrator.


Endpoints
^^^^^^^^^

There are two access points to the service.


1. Authorize: *https://ca-domain/authorize*

2. Revocation: *https://ca-domain/revocation-list*

Autorize
^^^^^^^^

.. csv-table::
   :header: "Method Allowed", "Parameters", "Responses"
   :widths: 5, 10, 30

   "POST", "csr=base64 string", "200/JSON: {crt: base64 string}"


This endpoint allows you to authorize a component within an organization.
Only one parameter called csr in base64 format is required.
If the data included in the CSR certificate is correct, the endpoint will return a 200 with a body in JSON format
containing the certificate also in base64 format:

.. code-block:: javascript

    {
        crt: -----BEGIN CERTIFICATE REQUEST-----
        MIICzDCCAbQCAQAwgYYxCzAJBgNVBAYTAkVOMQ0wCwYDVQQIDARub25lMQ0wCwYD
        VQQHDARub25lMRIwEAYDVQQKDAlXaWtpcGVkaWExDTALBgNVBAsMBG5vbmUxGDAW
        BgNVBAMMDyoud2lraXBlZGlhLm9yZzEcMBoGCSqGSIb3DQEJARYNbm9uZUBub25l
        LmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMP/U8RlcCD6E8AL
        PT8LLUR9ygyygPCaSmIEC8zXGJung3ykElXFRz/Jc/bu0hxCxi2YDz5IjxBBOpB/
        kieG83HsSmZZtR+drZIQ6vOsr/ucvpnB9z4XzKuabNGZ5ZiTSQ9L7Mx8FzvUTq5y
        /ArIuM+FBeuno/IV8zvwAe/VRa8i0QjFXT9vBBp35aeatdnJ2ds50yKCsHHcjvtr
        9/8zPVqqmhl2XFS3Qdqlsprzbgksom67OobJGjaV+fNHNQ0o/rzP//Pl3i7vvaEG
        7Ff8tQhEwR9nJUR1T6Z7ln7S6cOr23YozgWVkEJ/dSr6LAopb+cZ88FzW5NszU6i
        57HhA7ECAwEAAaAAMA0GCSqGSIb3DQEBBAUAA4IBAQBn8OCVOIx+n0AS6WbEmYDR
        SspR9xOCoOwYfamB+2Bpmt82R01zJ/kaqzUtZUjaGvQvAaz5lUwoMdaO0X7I5Xfl
        sllMFDaYoGD4Rru4s8gz2qG/QHWA8uPXzJVAj6X0olbIdLTEqTKsnBj4Zr1AJCNy
        /YcG4ouLJr140o26MhwBpoCRpPjAgdYMH60BYfnc4/DILxMVqR9xqK1s98d6Ob/+
        3wHFK+S7BRWrJQXcM8veAexXuk9lHQ+FgGfD0eSYGz0kyP26Qa2pLTwumjt+nBPl
        rfJxaLHwTQ/1988G0H35ED0f9Md5fzoKi5evU1wG5WRxdEUPyt3QUXxdQ69i0C+7
        -----END CERTIFICATE REQUEST-----
    }


the flow is

.. uml::

    participant "Application" as app
    participant "REST" as rest
    box "Ca Service" #LightBlue
    participant "REST" as ca_rest
    participant "validator" as ca_val
    participant "configuration" as ca_conf
    participant "generator" as ca_gen
    participant "keypair" as ca_pair
    end box

    app -> rest : POST
    rest -> ca_rest: authorize POST <<CSR>>
    ca_rest -> ca_val: validate CSR
    ca_val -> ca_conf: checkPK
    ca_conf --> ca_val: true/false
    ca_val -> ca_conf: checkOrg
    ca_conf --> ca_val: true/false
    ca_val --> ca_rest
    ca_rest -> ca_gen: signCrt
    ca_gen -> ca_pair: sign
    ca_pair --> ca_gen
    ca_gen --> ca_gen: create CRT
    ca_get --> ca_rest
    ca_rest --> rest: crt file
    rest --> app

Revocation
^^^^^^^^^

.. csv-table::
   :header: "Method Allowed", "Parameters", "Responses"
   :widths: 5, 10, 30

   "GET", "pk=udhudaq", "200/JSON:[{pk: asdfjkas}]"


This endpoint is used to obtain information about the certificates that have been revoked.
In a standard process, the Revocation List would correspond to a certificate that should be shared with the
network participants, but this has the complication that this list could be excessively large,
through a REST interface, however, it is possible to manage quickly also large lists of components and information.


the flow is

.. uml::

    participant "Application" as app
    participant "REST" as rest
    box "Ca Service" #LightBlue
    participant "REST" as ca_rest
    database "database" as ca_db
    end box

    app -> rest : GET/pk param
    rest -> ca_rest: revoke GET <<PK>>
    ca_rest -> ca_db: query the database
    ca_rest --> rest
    rest --> app

Certificate X509
----------------

The digital certificate provided by the CA is a stardard X509 certificate and is an electronic document used to prove
the ownership of a public key. The certificate includes information about the key, information about the identity of
its owner (called the subject), and the digital signature of the CA (called the issuer).

Common Fields
^^^^^^^^^^^^^

1. Serial Number: Used to uniquely identify the certificate within a CA's systems. In particular this is used to track revocation information.

2. Subject: The entity a certificate belongs to: a machine, an individual, or an organization.

3. Issuer: The entity that verified the information and signed the certificate.

4. Not Before: The earliest time and date on which the certificate is valid.

5. Not After: The time and date past which the certificate is no longer valid.

6. Key Usage: The valid cryptographic uses of the certificate's public key. Common values include digital signature validation, key encipherment, and certificate signing.

7. Extended Key Usage: The applications in which the certificate may be used. Common values include TLS server authentication, email protection, and code signing.

8. Public Key: A public key belonging to the certificate subject.

9. Signature Algorithm: The algorithm used to sign the public key certificate.

10. Signature: A signature of the certificate body by the issuer's private key.

Subject
^^^^^^^

the Subject field is a joint of information. Within the Subject are encountered the parameters that identify the
location and the type of component being authorized.

.. csv-table::
   :header: "Parameter", "Extended Version", "Description", "Mandatory"
   :widths: 5, 10, 30, 5

    "O", "Organization", "This parameter identifies the membership organization of the component", Yes
    "OU", "Organization Unit", "This parameter identifies the unit of membership within the organization. Accepted values: full_node, light_node, validator_node, oracle, bastion", Yes
    "CN", "Common Name", "This parameter identifies the name of the component", Yes
    "C", "Country", "This parameter identifies the Country of the component", No


CSR Certificate
^^^^^^^^^^^^^^^

A certificate signing request (also CSR or certification request) is a message sent from an applicant to a CA Service.

The CSR contains all the information necessary for the CA to identify and validate the component.

The information present in the Subject of the CSR will subsequently be used by the CA to create the Subject of the
certificate.

The fields used by this certificate are the standard ones of X509
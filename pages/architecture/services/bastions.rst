Bastions
========

The Breda blockchain network is made up of different organizations, each of which has a data structure
specific and above all, each has specific security and data distribution specifications.
It is therefore necessary to compartmentalize the data. In Breda this compatibility is achieved via hardware and not software.

Organization Structure
----------------------

With the Hardware compartmentation it will be a service, installed on a dedicated machine, which will control the flow
of incoming and outgoing data.

.. uml::

    package "Org 1" {
        actor "User 1" as user1
        usecase "Blockchain" as blockchain1
        usecase "Gateway" as gateway1

        user1 --> blockchain1
        blockchain1 --> gateway1
    }

    package "Org 2" {
        actor "User 2" as user2
        usecase "Blockchain" as blockchain2
        usecase "Gateway" as gateway2

        user2 --> blockchain2
        blockchain2 --> gateway2
    }

    package "Org 3" {
        actor "User 3" as user3
        usecase "Blockchain" as blockchain3
        usecase "Gateway" as gateway3

        user3 --> blockchain3
        blockchain3 --> gateway3
    }

    gateway1 <--> gateway2
    gateway2 <--> gateway3
    gateway1 <--> gateway3

With this type of architecture you will get a greater stability of the system.

The Gateway is the dedicated service that will mainly be in charge of:

- Share the steps of the PMO (subdivision of the SmartContract into sub-portions to be performed in different Organizations)
- Share the data structure of the various organizations
- Share data or portions of them
- Secure access to data

We will describe all of these processes in this document.


Authorization
--------------

Bastion is a Service that, at the network level, must be authorized like the others.
So it must:

- Create a certificate through the CA
- Request routing to Discovery to access the Organization.

In case of certificate creation it is necessary to configure the CSR with the field:


.. code-block:: console

    OU: bastion_service

Once the routing is completed, the certificate obtained from the CA must be shared in the organization's KAD network
in question.


.. uml::

    participant "Bastion" as bastion
    participant "CA" as ca
    participant "Discovery Service" as discovery
    participant "KAD" as kad

    bastion -> bastion: generate CSR
    bastion -> ca: generate CSR
    ca --> bastion
    bastion -> discovery: ask for routing
    discovery --> bastion: list of nodes
    bastion -> kad: share CSR

from this moment it is possible to identify the Bastion within the organization.

Load SmartContract PMO
------------------------

Bastion is a service that must be aware of the PMO "processes" that join the network.

Therefore, while not considering itself a validator node or a full node, Bastion must be informed
of events related to the PMO.

Bastion is connected to the Breda Blockchain network through Full-Nodes, but does not participate in the processes of
validation or execution of SmartContracts.

Registration with Full Node
^^^^^^^^^^^^^^^^^^^^^^^^^^^

synchronization with one or more full nodes occurs in different phases

the subscription:

In this phase the bastion warns a full node of its presence with an event


.. uml::

    participant "Bastion" as bastion
    participant "Full Node" as full
    participant "KAD" as kad

    bastion -> full: subscribe <<public key>>
    full -> kad: validate cert
    full --> bastion: subscription confirmed


Synchronization with Full Node
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once you have registered, you need to synchronize the Bastion.

The data to be synchronized are:

- Smart Contract processes (PMO process description)
- data model


SmartContract Sync
^^^^^^^^^^^^^^^^^^

Before seeing the synchronization process of the SmartContract it is necessary to discuss the composition of the latter.

A process is mainly composed of tasks, represented as:

.. code-block:: javascript

    <process value="Process One">
    ....
        <task>
            <org name="org2" id="1234" />
            <user pk="1234566" />
            <model name="engine" />
            <types>
                <id value="1" />
                <name value="Engine Aircraft" />
            </types>
            <action value="update" />
        </task>

we will not go into the explanation of all the components of the XML code just presented, the one that needs to be analyzed
I'm:

- org
- user
- model
- action

the * org * command indicates which organization it belongs to, with this command Bastion knows what else Bastion will owe
send the task

The * user * command, on the other hand, helps identify which user needs to perform the action or which user has the authority
to execute the command

The * model * command indicates the data model that will have to be shared between the two parties, bear in mind that it is not
it shares the complete set of information between one organization and another, but only part of the information. The commands
* types * indicate precisely what information there is to share between one bastion and another

Finally the command * action *, which indicates what action must be taken on the data

At the Bastion, every time a process is updated or added, the complete process arrives, the bastion will have to
read the XML code and identify, through the ORG command, which task must be shared

.. uml::

    participant "Full Node" as full
    participant "Bastion" as bastion
    participant "Others Bastions" as others

    full -> bastion: send process XML
    bastion -> bastion: extract external tasks
    bastion -> others: send task to the others

Il bastion send the following information:

.. code-block:: javascript

    <process name="Process One" origin="org1">
        <task>
            <org name="org2" id="1234" />
            <user pk="1234566" />
            <model name="engine" />
            <types>
                <id value="1" />
                <name value="Engine Aircraft" />
            </types>
            <action value="update" />
        </task>
    </process>

The bastion sends the process in a slightly modified way:

- delete the rest of the tasks that are not needed
- adds the origin field, which serves to identify which bastion it came from


The Bastion receiving the partial process must notify his organization's Blockchain network that it is necessary
add a new process.

.. uml::

    box "Organization One" #LightBlue
    participant "Bastion" as bastion_one
    end box
    participant "Kad" as kad
    box "Organization Two" #Orange
    participant "Bastion" as bastion
    participant "Full Node One" as full_one
    participant "Full Node Two" as full_two
    end box

    bastion_one -> bastion: send the partial process
    bastion -> kad: validate certificate
    bastion -> full_one: add new process xml
    full_one -> full_two: share process and validate


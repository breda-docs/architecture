Blockchain
============


Breda is a platform for distributed ledger solutions underpinned by a modular architecture delivering high
degrees of confidentiality, resiliency, flexibility, and scalability.

It is designed to support pluggable implementations of different components and accommodate the
complexity and intricacies that exist across the economic ecosystem.


What is a Blockchain?
---------------------

A distributed Ledger
^^^^^^^^^^^^^^^^^^^^

At the heart of a blockchain network is a distributed ledger that records all the transactions that take place on the network.

A blockchain ledger is often described as decentralized because it is replicated across many network participants,
each of whom collaborate in its maintenance.

We’ll see that decentralization and collaboration are powerful attributes that mirror the way businesses
exchange goods and services in the real world.

.. image:: /imgs/blockchain1.png

In addition to being decentralized and collaborative, the information recorded to a blockchain is append-only,
using cryptographic techniques that guarantee that once a transaction has been added to the ledger it cannot
be modified.

This property of “immutability” makes it simple to determine the provenance of information because participants
can be sure information has not been changed after the fact. It’s why blockchains are sometimes described as
systems of proof.

Smart Contracts
^^^^^^^^^^^^^^^

To support the consistent update of information — and to enable a whole host of ledger functions
(transacting, querying, etc) — a blockchain network uses smart contracts to provide controlled access
to the ledger.

.. image:: /imgs/blockchain2.png

Smart contracts are not only a key mechanism for encapsulating information and keeping it simple across the network,
they can also be written to allow participants to execute certain aspects of transactions automatically.

A smart contract can, for example, be written to stipulate the cost of shipping an item where the shipping
charge changes depending on how quickly the item arrives. With the terms agreed to by both parties and written
to the ledger, the appropriate funds change hands automatically when the item is received.

Consensus
^^^^^^^^^

The process of keeping the ledger transactions synchronized across the network — to ensure that ledgers update
only when transactions are approved by the appropriate participants, and that when ledgers do update,
they update with the same transactions in the same order — is called consensus.

.. image:: /imgs/blockchain3.png

You’ll learn a lot more about ledgers, smart contracts and consensus later. For now, it’s enough
to think of a blockchain as a shared, replicated transaction system which is updated via smart contracts
and kept consistently synchronized through a collaborative process called consensus.

Why is a Blockchain useful?
---------------------------

Today’s Systems of Record
^^^^^^^^^^^^^^^^^^^^^^^^^

The transactional networks of today are little more than slightly updated versions of networks that have
existed since business records have been kept. The members of a business network transact with each other,
but they maintain separate records of their transactions. And the things they’re transacting — whether it’s
Flemish tapestries in the 16th century or the securities of today — must have their provenance established
each time they’re sold to ensure that the business selling an item possesses a chain of title verifying their
ownership of it.

What you’re left with is a business network that looks like this:

.. image:: /imgs/blockchain4.png


Modern technology has taken this process from stone tablets and paper folders to hard drives and cloud platforms,
but the underlying structure is the same. Unified systems for managing the identity of network participants do not
exist, establishing provenance is so laborious it takes days to clear securities transactions (the world volume of
which is numbered in the many trillions of dollars), contracts must be signed and executed manually, and every
database in the system contains unique information and therefore represents a single point of failure.

It’s impossible with today’s fractured approach to information and process sharing to build a system of record
that spans a business network, even though the needs of visibility and trust are clear.

The Blockchain Difference
^^^^^^^^^^^^^^^^^^^^^^^^^

What if, instead of the rat’s nest of inefficiencies represented by the “modern” system of transactions,
business networks had standard methods for establishing identity on the network, executing transactions,
and storing data? What if establishing the provenance of an asset could be determined by looking through a
list of transactions that, once written, cannot be changed, and can therefore be trusted?

That business network would look more like this:

.. image:: /imgs/blockchain5.png

This is a blockchain network, wherein every participant has their own replicated copy of the ledger. In addition to
ledger information being shared, the processes which update the ledger are also shared. Unlike today’s systems,
where a participant’s private programs are used to update their private ledgers, a blockchain system has shared
programs to update shared ledgers.

With the ability to coordinate their business network through a shared ledger, blockchain networks can reduce
the time, cost, and risk associated with private information and processing while improving trust and visibility.

You now know what blockchain is and why it’s useful. There are a lot of other details that are
important, but they all relate to these fundamental ideas of the sharing of information and processes.
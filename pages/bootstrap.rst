Network Bootstrap
=================

On this Section we will describe a step by step process to create a Breda Network

Generic Services
----------------

for general services we indicate those services that are used by the blockchain network to obtain
information or that are used by applications directly.

The services in question are:

1. :doc:`Vault Service </pages/architecture/services/vault>`
2. :doc:`Orcale Service </pages/architecture/services/oracle>`
3. :doc:`Discovery Service </pages/architecture/services/discovery>`
4. :doc:`CA Service </pages/architecture/services/ca>`

First of all it will be necessary to install the authorization and authentication services, that is, the :doc:`CA Service </pages/architecture/services/ca>`
and the :doc:`Discovery Service </pages/architecture/services/discovery>`.

.. note:: These two services will give entry to the rest of the services and components of the network. We refer the configurations of these two services in the detailed description pages of each one

From here it will be possible to add general services.

The main difference with the rest of the network components is that the general services will not be authorized
in the network with a CSR process, but in the configuration phase a certificate will be added, signed by the CA,
for each generic service.

from the certificate the discovery is in charge of directing the service in the pertinent Organization.

Core Components
---------------

The components of the Core Network are:

1. :doc:`Validation Node </pages/architecture/node>`
2. :doc:`Full Node </pages/architecture/node>`
3. :doc:`Light Node </pages/architecture/node>`
4. :doc:`Bastion Service </pages/architecture/services/bastions>`


Node Registration
^^^^^^^^^^^^^^^^^

.. uml::

    participant "Node" as node
    participant "Discovery Service" as disc
    participant "Organization" as org

    node -> disc : send CRT
    disc --> node: partial list of peers
    node -> org: node accepted into the organization


During the registration process with :doc:`Discovery Service </pages/architecture/services/discovery>`,
the node receives a partial list of neighbors.

It is necessary that the node is aware of the nodes in the network, for that, instead of receiving a huge
list from :doc:`Discovery Service </pages/architecture/services/discovery>`, the
:doc:`Node </pages/architecture/node>`, once in the Organization, effects a dissemination of its neighbor list
through the :doc:`Gossip Protocol </pages/architecture/protocols/gossip>`

.. uml::

    database "Table Neig" as table
    participant "New Node" as node
    participant "Neighbour 1" as neig_one
    participant "Neighbour 2" as neig_two
    participant "Neighbour 3" as neig_three

    node -> neig_one : send neig. list <<version 0>>
    note right : Start the Gossip Dissemination
    node -> neig_two : send neig. list <<version 0>>
    node -> neig_three : send neig. list <<version 0>>

    neig_one --> node: list <<version 1>>
    node -> table: update the list

    neig_one --> node: list <<version 1>>

    neig_three --> node: list <<version 3>>

    node -> table: update list


The node starts by sending its neighbor list to its neighbors.

They will reply with the updated list.

Each node list has a specific version, if the new :doc:`Node </pages/architecture/node>` has a lower list version than
the one it receives from the neighboring nodes, the new node will update its table, in this way with very few passages
it will be possible to offer visibility of the network to all network nodes and services.

In the process of updating the neighbor table it is also essential to validate or authenticate the service/node that is
sending the information.

The process is through digital certificates.

There are two ways to perform the validation:

1. In each message the component sends, the certificate together with the content of the message
2. A :doc:`DHT </pages/architecture/protocols/kad>`

Obviously method 1 is not viable, because it increases the dimensions of each of the messages and also each component
would have to store all or part of the certificates. For this reason we are going to use a
:doc:`DHT </pages/architecture/protocols/kad>` where, searching by public key,
the digital certificate of the service/node will be obtained